from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf.urls.static import static

from cloud.apps.account.views import LoginView, SignupView

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    url(r'^contact/$', TemplateView.as_view(template_name='contact.html'), name='contact'),
    url(r'^faq/$', TemplateView.as_view(template_name='faq.html'), name='faq'),
    url(r'^pricing/$', TemplateView.as_view(template_name='pricing.html'), name='pricing'),
    url(r'^services/$', TemplateView.as_view(template_name='services.html'), name='services'),
    url(r'^agreement/$', TemplateView.as_view(template_name='agreement.html'), name='agreement'),

    url(r"^welcome$", "cloud.apps.account.views.welcome_signup", name="welcome_signup"),
    url(r"^logout$", "cloud.apps.account.views.logout", name="logout"),
    url(r"^account/login/$", LoginView.as_view(), name="account_login"),
    url(r"^account/signup/$", SignupView.as_view(), name="account_signup"),
    url(r"^account/", include("account.urls")),

    url(r"^feed/", include("cloud.apps.notification.urls")),
    url(r"^enterprise/", include("cloud.apps.product.urls")),


    url(r"^enterprise/", include("cloud.apps.enterprise.urls")),

    url(r"^my-enterprises/$", "cloud.apps.dashboard.views.my_enterprises", name="my_enterprises"),
    #url(r'^my-enterprises/(?P<nama_toko>\w+)/product-family$', 'cloud.apps.product.views.product_family', name='product_family'),
    #url(r'^my-enterprises/(?P<nama_toko>\w+)/edit$', 'cloud.apps.enterprise.views.edit_enterprise', name='edit_enterprise'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)