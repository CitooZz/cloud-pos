from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from cloud.apps.product.models import Product
from cloud.apps.product.forms import ProductForm
from cloud.apps.enterprise.models import Enterprise


@login_required
def product_submit(request, slug, product_id=None):
	enterprise = get_object_or_404(Enterprise, slug=slug)
	product = None
	if product_id:
		product = get_object_or_404(Product, id=product_id)

	form = ProductForm(instance=product)
	context = {
		'enterprise': enterprise,
		'form': form
	}
	return render(request, "product/create_product.html", context)


@login_required
def browse_product(request, slug):
    enterprise = get_object_or_404(Enterprise, slug=slug)
    context = {
    	'enterprise': enterprise
    }

    return render(request, "product/browse_product.html", context)
