from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from cloud.apps.enterprise.models import Enterprise
from autoslug import AutoSlugField


def product_logo_upload_handler(instance, filename):
    return "product/%s/logo-%s" % (instance.slug, filename)


class Category(models.Model):
    parent = models.ForeignKey('self', related_name="categories", null=True, blank=True)
    name = models.CharField(max_length=80)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Variant(models.Model):
    name = models.CharField(max_length=80)
    value = models.CharField(max_length=80)

    def __unicode__(self):
        return self.name


class Product(models.Model):
    enterprise = models.ForeignKey(Enterprise, related_name='products')
    creator = models.ForeignKey(User)
    name = models.CharField(_('Nama'), max_length=80)
    slug = AutoSlugField(populate_from="name", unique=True)
    logo = models.ImageField(upload_to=product_logo_upload_handler)
    barcode = models.CharField(_('Barcode'), max_length=80)
    description = models.TextField(_('Deskripsi'))
    category = models.ForeignKey(Category)
    min_stock = models.IntegerField(default=0)
    stock = models.IntegerField(default=0)
    is_all = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def is_all(self):
        return self.is_all
