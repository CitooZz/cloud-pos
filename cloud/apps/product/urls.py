from django.conf.urls import patterns, url

urlpatterns = patterns(
    'cloud.apps.product.views',

    url(r'^([-\w.]+)/product/submit/$', 'product_submit', name='product_submit'),
    url(r'^([-\w.]+)/product/$', 'browse_product', name='browse_product'),
)

