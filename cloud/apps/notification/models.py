from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags


class NoticeCategory(models.Model):
    name = models.CharField("Name", max_length=50)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("Notice Category")
        verbose_name_plural = _("Notice Categories")


class NoticeType(models.Model):
    label = models.CharField(_("label"), max_length=50)
    display = models.CharField(_("display"), max_length=50)
    enabled = models.BooleanField(_("enabled"), default=True)
    category = models.ForeignKey(
        NoticeCategory, related_name="notice_category")

    def __unicode__(self):
        return self.label

    class Meta:
        verbose_name = _("Notice Type")
        verbose_name_plural = _("Notice Types")


class NoticeSetting(models.Model):
    user = models.ForeignKey(User, verbose_name=_("user"))
    notice_type = models.ForeignKey(NoticeType, verbose_name=_("notice type"))
    email = models.BooleanField(_("email"), default=True)
    site = models.BooleanField(_("site"), default=True)

    class Meta:
        verbose_name = _("notification setting")
        verbose_name_plural = _("notification setting")
        unique_together = ("user", "notice_type")


class Notice(models.Model):
    user = models.ForeignKey(User, verbose_name=_("user"))
    creator = models.ForeignKey(User, verbose_name=_(
        "creator"), null=True, related_name="created_notifications")
    message = models.TextField()
    notice_type = models.ForeignKey(NoticeType, verbose_name=_("notice type"))
    added = models.DateTimeField(_("added"), auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def __unicode__(self):
        return self.message

    @property
    def clean_message(self):
        message = strip_tags(self.message).split("\n")
        return ' '.join(message)


def get_formatted_messages(formats, label, context):

    format_templates = {}
    for format in formats:
        if format.endswith(".txt"):
            context.autoescape = False
        else:
            context.autoescape = True

        format_templates[format] = render_to_string(
            ("notification/%s/%s" % (label, format),
                "notification/%s/%s" % format), context_instance=context)

    return format_templates


def send(users, label, creator, extra_context=None):
    if extra_context is None:
        extra_context = {}

    notice_type = NoticeType.objects.get(label=label)
    formats = {
        "email_subject.txt",
        "email_content.html"
    }

    current_site = Site.objects.get_current()
    for user in users:
        context = Context({
            "receiver": user,
            "current_site": current_site,
            "STATIC_URL": settings.STATIC_URL,
            "creator": creator
        })
        context.update(extra_context)
        messages = get_formatted_messages(formats, label, context)
        setting = NoticeSetting.objects.get_or_create(
            user=user, notice_type=notice_type)[0]

        if setting.email:
            body = messages["email_content.html"]
            email = EmailMultiAlternatives(
                messages["email_subject.txt"], settings.DEFAULT_FROM_EMAIL, [user.email])
            email.attach_alternative(body, "text/html")
            email.send()
