from django.conf.urls import patterns, url


urlpatterns = patterns("cloud.apps.notification.views",
    url(r"^$", "notifications", name="notifications"),
)