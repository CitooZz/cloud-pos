from django import forms
from django.utils.translation import ugettext_lazy as _

import account.forms


class SignupForm(account.forms.SignupForm):
    first_name = forms.CharField(
        label=_("First Name"), required=True, max_length=50)
    last_name = forms.CharField(
        label=_("Last Name"), required=True, max_length=50)

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        del self.fields["username"]
