from django.template.defaultfilters import slugify
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as default_logout
from django.core.urlresolvers import reverse
from cloud.apps.account.forms import SignupForm
import account.forms
import account.views


class LoginView(account.views.LoginView):
    form_class = account.forms.LoginEmailForm


class SignupView(account.views.SignupView):
    form_class = SignupForm
    messages = {
        "email_confirmation_sent": {
            "level": messages.INFO,
            "text": _("Your account has been created.")
        },
        "invalid_signup_code": {
            "level": messages.WARNING,
            "text": _("The code %(code)s is invalid.")
        }
    }

    def generate_username(self, form):
        # After signup username will be overwrite, see method below
        email = form.cleaned_data['email'].strip()
        username = email.replace('@', '-').replace('.', '-')
        return username

    def after_signup(self, form):
        after_create_user(form, self.created_user)
        super(SignupView, self).after_signup(form)


def after_create_user(form, user):
    username = '%s-%s' % (slugify(form.cleaned_data['first_name']), user.id)
    user.username = username
    user.save()

    # initial user profile
    profile = user.get_profile()
    profile.full_name = form.cleaned_data['first_name'] + " " + \
        form.cleaned_data['last_name']
    profile.save()


@login_required
def welcome_signup(request):
    context = {}
    return render(request, "account/welcome_signup.html", context)


@login_required
def logout(request):
    default_logout(request)
    return redirect(reverse("account_login"))