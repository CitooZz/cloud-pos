from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from cloud.apps.enterprise.models import Enterprise

@login_required
def my_enterprises(request):
	enterprises = Enterprise.objects.filter(owner=request.user)
	context = {
		'enterprises': enterprises
	}
	return render(request, "dashboard/my-enterprises.html", context)