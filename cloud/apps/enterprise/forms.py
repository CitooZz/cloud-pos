from django import forms
from cloud.apps.enterprise.models import Enterprise


class CreateEnterpriseForm(forms.ModelForm):

    class Meta:
        model = Enterprise
        fields = ("name", "logo", "description", "address",
                  "phone", "zipcode", "website", "industry")

    def __init__(self, *args, **kwargs):
        super(CreateEnterpriseForm, self).__init__(*args, **kwargs)

        for field_name in self.fields:
            self.fields[field_name].widget.attrs['class'] = 'col-md-8'
