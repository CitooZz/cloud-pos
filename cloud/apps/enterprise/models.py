from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinLengthValidator
from django.core.urlresolvers import reverse
from autoslug import AutoSlugField


def enterprise_logo_upload_handler(instance, filename):
    return "enterprise/%s/logo-%s" % (instance.slug, filename)


class Industry(models.Model):
	name = models.CharField(max_length=100)
	slug = AutoSlugField(populate_from="name")

	class Meta:
		ordering = ["name"]

	def __unicode__(self):
		return self.name


class Enterprise(models.Model):
    owner = models.ForeignKey(User, related_name="enterprises")
    name = models.CharField(
        _("Nama"), max_length=30, unique=True, validators=[MinLengthValidator(5)])
    slug = AutoSlugField(populate_from="name", unique=True)
    subdomain = models.CharField(_("Sub Domain"), max_length=50)
    logo = models.ImageField(upload_to=enterprise_logo_upload_handler)
    description = models.TextField(_("Deskripsi"))
    address = models.CharField(_("Alamat"), max_length=200)
    phone = models.CharField(_("Telefon"), max_length=20)
    zipcode = models.CharField(_("Kode pos"), max_length=10)
    website = models.URLField(_("Website"), blank=True)
    industry = models.ForeignKey(Industry, related_name="enterprises", null=True, blank=True)

    def __unicode__(self):
    	return self.name

    def get_absolute_url(self):
    	return reverse("enterprise_detail", args=[self.slug])
