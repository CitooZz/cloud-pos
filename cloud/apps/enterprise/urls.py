from django.conf.urls import patterns, url


urlpatterns = patterns("cloud.apps.enterprise.views",
   url(r"^create/$", "create_enterprise", name="create_enterprise"),
   url(r"^([-\w.]+)/edit/$", "create_enterprise", name="edit_enterprise"),
   url(r"^([-\w.]+)/detail/$", "enterprise_detail", name="enterprise_detail"),
)