from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from cloud.apps.enterprise.models import Enterprise, Industry
from cloud.apps.enterprise.forms import CreateEnterpriseForm


@login_required
def create_enterprise(request, slug=None):
    enterprise = None
    if slug:
        enterprise = get_object_or_404(Enterprise, slug=slug)

    if request.POST:
        form = CreateEnterpriseForm(
            request.POST, request.FILES, instance=enterprise)
        if form.is_valid():
            enterprise = form.save(commit=False)
            enterprise.owner = request.user
            enterprise.save()

            messages.success(request, "Success")
            return redirect(reverse('my_enterprises'))
    else:
        form = CreateEnterpriseForm(instance=enterprise)

    context = {
        'form': form,
        'industries': Industry.objects.all(),
    }
    return render(request, "enterprise/create_enterprise.html", context)


@login_required
def enterprise_detail(request, slug):
    enterprise = get_object_or_404(Enterprise, slug=slug)
    context = {
        'enterprise': enterprise
    }
    return render(request, "enterprise/enterprise_detail.html", context)
