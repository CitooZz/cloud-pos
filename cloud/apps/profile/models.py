from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save


class Profile(models.Model):

    class Meta:
        app_label = "profile"

    user = models.ForeignKey(User, unique=True, verbose_name=_("user"))
    full_name = models.CharField(_("Full Name"), max_length=40, blank=True)
    company = models.CharField(_("Company"), max_length=200, blank=True)

    # Facebook Id & Token
    facebook_id = models.CharField(max_length=20, blank=True)
    facebook_token = models.TextField(blank=True)

    daily_notification = models.BooleanField(default=False)
    terms_accepted = models.BooleanField(default=True)

    @models.permalink
    def get_absolute_url(self):
        return "profile_detail", [self.user.username]


def create_profile_for_new_user(sender, instance, **kwargs):
    if instance is None:
        return

    profile, created = Profile.objects.get_or_create(user=instance)
    if created:
        profile.full_name = "%s %s" % (instance.first_name, instance.last_name)
        profile.save()

post_save.connect(create_profile_for_new_user, sender=User)
