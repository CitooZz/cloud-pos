from django import template

register = template.Library()


@register.inclusion_tag("unify_top_menu.html")
def unify_top_menu(active_item):
    return {"active_item": active_item}


@register.inclusion_tag("dashboard/dashboard_left_menu.html", takes_context=True)
def dashboard_left_menu(context, active_menu, active_item):
    context['active_item'] = active_item
    context['active_menu'] = active_menu
    return context


@register.inclusion_tag("enterprise/enterprise_left_menu.html", takes_context=True)
def enterprise_left_menu(context, active_menu, active_item):
    context['active_item'] = active_item
    context['active_menu'] = active_menu
    return context
