from django import template
from django import forms
from django.forms import BooleanField, ImageField, TypedChoiceField
from django.forms.forms import BoundField
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(name="bootstrap_field")
def bootstrap_field(field):
    if not isinstance(field, BoundField):
        return ''

    klass = field.field.widget.attrs.get('class', '')
    if type(field.field.widget) != forms.CheckboxInput:
        field.field.widget.attrs['class'] = "%s %s" % (klass, 'form-control')

    if type(field.field.widget) == forms.Select:
        field.field.widget.attrs['class'] = klass + ' form-control select2'

    if field.field.required:
        field.field.widget.attrs['required'] = ''

    context = {'field': field}
    return render_to_string("form/field.html", context)