import urlparse
from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def inner_link(context, url):
    """
    return '' if the url is same to request.path

    @param context: TemplateContext
    @param url: the url
    @return: url if not request.path == url else ""
    """

    request_path = context['request'].path
    if urlparse.urlparse(url).path == request_path:
        return ''

    return url

@register.filter('klass')
def klass(ob):
    return ob.__class__.__name__