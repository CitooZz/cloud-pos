from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User


class MessageManager(models.Manager):

    def message_for(self, user, contact):
        return self.filter(Q(sender=contact, recipient=user, hide_for_recipient=False) |
                           Q(recipient=contact, sender=user, hide_for_sender=False)).order_by("received_at")


class Message(models.Model):
    sender = models.ForeignKey(User, related_name="sent_messages")
    recipient = models.ForeignKey(User, related_name="recieved_messages")
    recieved_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    is_read = models.BooleanField(default=False)
    hide_for_sender = models.BooleanField(default=False)
    hide_for_recipient = models.BooleanField(default=False)
    objects = MessageManager()

    def __unicode__(self):
        return self.content


class MessageStatusManager(models.Manager):

    def create_or_update(self, sender, recipient, message):
        status = self.get_or_create(user=sender, contact=recipient)[0]
        status.last_message = message
        status.save()

        status_2 = self.get_or_create(user=recipient, contact=sender)[0]
        status_2.last_message = message
        status_2.save()


class MessageStatus(models.Model):
    user = models.ForeignKey(User, related_name="user_status")
    contact = models.ForeignKey(User, related_name="contact_status")
    last_message = models.ForeignKey(Message related_name="message_status_item", null=True, blank=True)

    objects = MessageStatusManager()
